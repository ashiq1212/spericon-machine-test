import {Injectable} from '@angular/core';
import {HttpClient, } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {


  constructor(private httpClient: HttpClient) {
  }

  PostRegister(data) {
    return this.httpClient.post('/auth/register', data);
  }

  LoginAuth(creds) {
    return this.httpClient.post('/auth/login',creds);
  }

  GetProfile() {
    return this.httpClient.get('/user');
  }

}
