import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AppService} from '../services/app.service'
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	LoginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
  			private appService:AppService,
  			private router:Router) 
 	{ }

  get f() {
    return this.LoginForm.controls;
  }

  ngOnInit() {
  	this.LoginForm = this.formBuilder.group({
  		email : ['', Validators.required],
  		password : ['', Validators.required]
  	})
  }

  onLogin(){
  	if(this.LoginForm.invalid){
  		alert('Please Fill Password and Username')
  		return
  	}

  	this.appService.LoginAuth(this.LoginForm.value).subscribe(resp=>{
  		if(resp['success']){
  			localStorage.setItem('login_token',resp['data']['token'])
  			this.router.navigate(['/dashboard'])
  		}else{
  			alert(resp['message'])
  		}

  	})
  	
  }

}
