import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import * as NProgress from 'nprogress';
import {environment} from '../../environments/environment';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(
  	private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const serverurl = environment.serverUrl;

    req = req.clone({url: serverurl + req.url});

    const idToken = localStorage.getItem('login_token');
    if (idToken) {
      req = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + idToken)
      });
    }

    return next.handle(req).pipe(
      catchError(err => {
        // if (err.status === 503) {
        //   alert('error')
        // } else 
        if (err.status === 401) {
          location.reload(true);
        }
        return throwError(err);
      }),
      tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
           
          }
        },
        (err: any) => {
        }
      )
    );
  }
}
