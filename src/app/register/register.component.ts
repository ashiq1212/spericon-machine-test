import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AppService} from '../services/app.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  RegisterForm: FormGroup;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  password_special_character = '^(?=.*?[#?!@$%^&*-]).*';
  phonePattern = '[0-9]+';


  constructor(private formBuilder: FormBuilder,
  			private appService:AppService,
  			private router:Router) 
 	{ }

  get f() {
    return this.RegisterForm.controls;
  }

  formCreate(){
    this.RegisterForm = this.formBuilder.group({
      username:['',Validators.required],
      phone:['',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern(this.phonePattern)]],
      email:['',[Validators.required, Validators.pattern(this.emailPattern)]],
      password:['',[Validators.required,Validators.pattern(this.password_special_character),Validators.minLength(8)]],
      confirmedPassword : [null, Validators.required]
    },
      {
        validator: this.ConfirmPasswordMatch("password", "confirmedPassword")
      }
    )
  }

  ConfirmPasswordMatch(password_input: string, confirmedPassword_input: string) {
  	return (formGroup: FormGroup) => {
        const password = formGroup.controls[password_input];
        const confirmedPassword = formGroup.controls[confirmedPassword_input];

        if (confirmedPassword.errors && !confirmedPassword.errors.mustMatch) {
            return;
        }
        if (password.value !== confirmedPassword.value) {
            confirmedPassword.setErrors({ mustMatch: true });
        } else {
            confirmedPassword.setErrors(null);
        }
    }
  }

  ngOnInit() {
  	this.formCreate()
  }



  onRegister(){
  	if(this.RegisterForm.invalid){
  		alert('Please Fill all the fields!')
  		return
  	}
  	let register = this.appService.PostRegister(this.RegisterForm.value)
  	register.subscribe(resp=>{
      alert(resp['message'])
      if(resp['success']){
       this.router.navigate(['/login'])
      }
  	 	
  	})

  }

}
