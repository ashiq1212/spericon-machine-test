import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NavComponent } from './nav/nav.component'

import { MatCardModule} from '@angular/material/card';

@NgModule({
  declarations: [DashboardComponent, NavComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatCardModule
  ]
})
export class DashboardModule { }
