import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard.component';

const routes: Routes = [
	{ path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
	{ path : 'dashboard', component : DashboardComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
