import { Component, OnInit } from '@angular/core';
import { AppService} from '../../services/app.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private appService:AppService,
  			private router:Router) 
 	{ }

   user_profile:any ={}

  ngOnInit() {
  	this.appService.GetProfile().subscribe(resp=>{
  		if(resp['success']){
  			this.user_profile = resp['data']['userData']
  		}else{
  			alert(resp['message'])
  		}
  	})
  }

}
